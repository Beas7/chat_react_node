var mysql = require("mysql");

var con = mysql.createConnection({
  host: "localhost",
  user: "datChatRoot",
  password: "p@sSw0rd",
  database: "datChat",
});

function storeData(query) {
  con.query(query, function (err, result) {
    if (err) throw err;
  });
}

/*
  create table messageHistory (
    id int(5) auto_increment primary key,
    author varchar(255),
    room varchar(255),
    message varchar(255),
    created_at datetime
  );
*/
module.exports = storeData;
