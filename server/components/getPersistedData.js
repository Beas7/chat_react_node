var mysql = require("mysql");

var con = mysql.createConnection({
  host: "localhost",
  user: "datChatRoot",
  password: "p@sSw0rd",
  database: "datChat",
});

function getPersistedData(query, roomCount) {
  return new Promise((resolve, reject) => {
    con.query(query, (err, result) => {
      return err ? reject(err) : resolve(result);
    });
  });
}

module.exports = getPersistedData;
