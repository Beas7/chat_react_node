const express = require("express");
const http = require("http");
const app = express();
const server = http.createServer(app);
const storeData = require("./components/persister");
const getPersistedData = require("./components/getPersistedData");
const dateTimeFormatter = require("./components/dateTimeFormatter");

// Edit CORS to allow specific origins. You can add and use https://www.npmjs.com/package/cors if you want a detailed API provider for that
const io = require("socket.io")(server, { cors: { origin: "*" } });

// TODO: securize the app with the preferred method

io.on("connection", (socket) => {
  socket.on("join_room", (roomName) => {
    socket.join(roomName);
    console.log(`User joined the room ${roomName}`);
    // send message history to hydratate the client
    let query = `select * from messageHistory where room = ${roomName} order by created_at;`;
    (async () => {
      let data = await getPersistedData(query);
      socket.emit("hydratate", data);
      console.log("event emmited");
    })();
  });

  socket.on("message", (data) => {
    socket.to(data.room).emit("messages", data);
    // persist data
    query = `insert into messageHistory (author, room, message, created_at) values (
      '${data.author}', '${data.room}', '${data.message}', '${dateTimeFormatter()}')`;
    // TODO: protect and escape data before inserting, i.e. see node-mysql-native lib

    storeData(query);
  });
});

// set desired PORT env var in each environment's system path, otherwise it'll fallback to 5000, you can change it as you like
const port = process.env.PORT || 5000;
// start the service and prompt the currently used port on server's console
server.listen(port, () => console.log(`server running on port ${port}`));
