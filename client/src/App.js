import "./App.css";
import React, { useState } from "react";
import Chat from "./components/chat";

function App() {
  const [userName, setUserName] = useState("");
  const [registeredUser, setRegistered] = useState(false);
  const [roomName, setRoomName] = useState("");
  const [registeredRoom, setRegisteredRoom] = useState(false);

  function registerUser(userName) {
    if (userName !== "") setRegistered(true);
  }

  function registerRoom(roomName) {
    if (roomName !== "") setRegisteredRoom(true);
  }

  // get userName from url queryString to assign it automatically
  const params = new URLSearchParams(window.location.search);
  if (!registeredUser && params.has("userName")) {
    setUserName(params.get("userName"));
    registerUser(userName);
    if (!registeredRoom && params.has("room")) {
      setRoomName(params.get("room"));
      registerRoom(roomName);
    }
  }

  return (
    <div className="App">
      {registeredUser && registeredRoom && <Chat userName={userName} roomName={roomName}></Chat>}
    </div>
  );
}

export default App;
