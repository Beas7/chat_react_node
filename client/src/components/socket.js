import io from "socket.io-client";
// set up the server port in your env path, otherwise it'll use localhost:5000 which is the fallback on our server
const server = process.env.CHAT_SERVER || "http://localhost:5000";

const socket = io(server);

export default socket;
