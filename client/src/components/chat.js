import React, { useState, useEffect, useRef } from "react";
import socket from "./socket";
import getCurrentDate from "./dateTimeFormatter";

const Chat = ({ userName, roomName }) => {
  const [message, setMessage] = useState("");
  const [messageHistory, setMessageHistory] = useState([]);
  const [dataFetched, setDataFetched] = useState(false);

  useEffect(() => {
    socket.emit("join_room", roomName);
  }, [roomName]);

  useEffect(() => {
    socket.on("messages", (data) => {
      setMessageHistory([...messageHistory, data]);
    });

    socket.on("hydratate", (data) => {
      if (!dataFetched) {
        setMessageHistory([...messageHistory, ...data]);
        setDataFetched(true);
      }
      console.log(dataFetched);
    });
    // avoid message loop:
    return () => {
      socket.off();
    };
  }, [messageHistory, dataFetched]);

  const divRef = useRef(null);
  useEffect(() => {
    divRef.current && divRef.current.scrollIntoView({ behavior: "smooth" });
  });

  const submit = (e) => {
    e.preventDefault();
    let messageContent = {
      room: roomName,
      author: userName,
      message: message,
      created_at: getCurrentDate(),
    };
    socket.emit("message", messageContent);
    setMessageHistory([...messageHistory, messageContent]);
    setMessage("");
  };

  return (
    dataFetched && (
      <div className="container">
        <h1>Dat Chat</h1>
        <div className="room">claim: {roomName}</div>
        <div className="chat-history-container">
          <div className="chat-history">
            {messageHistory.map((val, key) => (
              <div className={val.author === userName ? "current-user" : "not-current-user"} key={key}>
                <div className="user-name">{val.author}: </div>
                <div className="message" data-attribute={val.created_at}>
                  <div className="content">{val.message}</div>
                </div>
                <div ref={divRef}></div>
              </div>
            ))}
          </div>
        </div>
        <form id="chat-form" onSubmit={submit}>
          <label htmlFor="text-box">Message: </label>
          <div className="typing-box">
            <textarea id="text-box" value={message} onChange={(e) => setMessage(e.target.value)}></textarea>
            <button className="send-btn" title="Send message">
              <svg
                aria-hidden="true"
                focusable="false"
                data-prefix="fab"
                data-icon="telegram-plane"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                className="send-icon">
                <path
                  fill="currentColor"
                  className="send-icon"
                  d="M446.7 98.6l-67.6 318.8c-5.1 22.5-18.4 28.1-37.3 17.5l-103-75.9-49.7 47.8c-5.5 5.5-10.1 10.1-20.7 10.1l7.4-104.9 190.9-172.5c8.3-7.4-1.8-11.5-12.9-4.1L117.8 284 16.2 252.2c-22.1-6.9-22.5-22.1 4.6-32.7L418.2 66.4c18.4-6.9 34.5 4.1 28.5 32.2z"></path>
              </svg>
            </button>
          </div>
        </form>
      </div>
    )
  );
};

export default Chat;
