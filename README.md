## Run the project

For development:

<pre>
$ cd ./server
$ npm install
$ npm run watch
</pre>

<pre>
$ cd ./client
$ npm install
$ npm run start
</pre>

For production:

<pre>
$ cd ./server
$ npm install
$ npm run start
</pre>
<pre>
$ cd ./server
$ npm install
$ npm run build
</pre>
